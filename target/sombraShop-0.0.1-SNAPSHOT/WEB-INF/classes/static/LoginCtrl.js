angular.module('shop').controller('LoginCtrl', function($http, $scope, $rootScope){
/*    console.log(email)*/
        $scope.submit = function(loginForm){
            $rootScope.myProfile = loginForm.email.$viewValue;
            $http.post('/user', {email: loginForm.email.$viewValue, password: loginForm.password.$viewValue}).success(function(data){
                if (data == "BAD_REQUEST") {
                    $rootScope.authenticated = false;
                } else {
                    $rootScope.authenticated = true;
            }
        })
        }

        $scope.logout = function() {
            $rootScope.authenticated = false;
            $rootScope.cart.clearCart();
        }
})