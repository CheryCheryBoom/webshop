angular.module('shop').factory('CartService', ['$cookieStore','$http','$state', function(cookies, $http, $state){
    var cart = {
        itemsCookie: '',

        init: function(itemsCookie){
            this.itemsCookie = itemsCookie;

            if(!(cookies.get(this.itemsCookie) instanceof Array)){
                cookies.put(this.itemsCookie, [])
            };
        },

        addItem: function(item){
            var items = cookies.get(this.itemsCookie);
            var existed = false;
            for(var i = 0; i < items.length; i++) {
                if(items[i].id == item.id) {
                    items[i].quantity += 1;
                    existed = true;
                }
            }
            if(!existed) {
                items.push({
                    id: item.id,
                    name: item.name,
                    price: item.price,
                    quantity: 1
                });
            }
            cookies.put(this.itemsCookie, items);
        },

/*        isExisted: function(items, id){
            var existed = false;
            angular.forEach(items, function(){
                if(items.id == id) existed = true
            })
            return existed;
        },*/

        getAll: function(){
            return cookies.get(this.itemsCookie);
        },

        getQuantity: function(item){
            var items = cookies.get(this.itemsCookie);
            for(var i = 0; i < items.length; i++) {
                if(items[i].id == item.id) return items[i].quantity
            }

        },

        getPrice: function(item){
            var items = cookies.get(this.itemsCookie);
            for(var i = 0; i < items.length; i++) {
                            if(items[i].id == item.id) return items[i].price*items[i].quantity
                        }
        },

        removeItem: function(item) {
            var items = cookies.get(this.itemsCookie);
            for(var i = 0; i < items.length; i++) {
                    if(items[i].id == item.id) {
                        items[i].quantity -= 1;
                    }
                    if(items[i].quantity == 0) items.splice(i, 1);
            }

            cookies.put(this.itemsCookie, items);
        },

        totalItems: function(){
            var quantity = 0, items = cookies.get(this.itemsCookie);
            for(var i = 0; i < items.length; i++){
                quantity += items[i].quantity;
            }
            return quantity;
        },

        totalPrice: function(){
            items = cookies.get(this.itemsCookie);
            var price = 0;
            for(var i = 0; i < items.length; i++) {
                price += items[i].price*items[i].quantity;
            }
            return price;
        },

        clearCart: function(){
/*            var items = cookies.get(this.itemsCookie);
            i*/
            cookies.put(this.itemsCookie, []);
        },

        submitCart: function(myProfile){
            var items = cookies.get(this.itemsCookie, []);
            console.log(myProfile);
            $http.post('/cart', {email: myProfile, product: items}).success(function(data){
                console.log(data);
                /*cookies.put(this.itemsCookie, []);*/
/*                this.clearCart();*/
                $state.go('cart.success');
            }).error(function (data){
                console.log(data);
                $state.go('cart.error');
            })
        }
    }

    return cart;
}])