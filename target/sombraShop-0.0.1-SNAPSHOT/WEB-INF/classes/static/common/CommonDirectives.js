angular.module('shop').directive('productsByCategory', function(){
    return {
        restrict: 'A',
        require: 'ngModel',
        controller: function($scope, $state){
            $scope.viewDetails = function(category) {
                $state.go('products', {category: category})
            }
        }
    }
});
angular.module('shop').directive('loginForm', function(){
    return {
        restrict: 'E',
        require: 'ngModel',
        templateUrl: 'common/directive/loginForm.html',
        controller: 'LoginCtrl',

    }
})