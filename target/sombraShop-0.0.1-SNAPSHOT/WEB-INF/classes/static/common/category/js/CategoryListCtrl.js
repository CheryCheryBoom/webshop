angular.module('shop').controller('CategoryListCtrl', function($scope, $http) {
    $http.get('/category').success(function(data) {
      $scope.categories = data._embedded.category;
    })
  })