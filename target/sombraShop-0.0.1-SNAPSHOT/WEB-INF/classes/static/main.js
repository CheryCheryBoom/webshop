var shop = angular.module('shop', ['restangular', 'ui.router', 'ngCookies' ])
  .config(function($stateProvider, $urlRouterProvider, $httpProvider) {
    $urlRouterProvider.otherwise("/")
	$stateProvider
        .state('category', {
          url: "/",
          templateUrl: "common/category/list.html",
          controller: "CategoryListCtrl"
        })
        .state('cart', {
          url: "/cart",
          templateUrl: "/common/cart/cart.html",
          module: 'private',
          controller: "CartCtrl"
        })
        .state('cart.success',{
            url:"/success",
            views: {
                "@" : {
                    templateUrl: "common/cart/success.html",
                    controller: function($rootScope){
                        $rootScope.cart.clearCart();
                    }
                }
            }
        })
        .state('cart.error',{
                    url:"/error",
            views: {
                "@" : {
                templateUrl: "common/cart/error.html"
                }
            }
         })
        .state('products', {
          url: "/:category",
          templateUrl: "common/product/list.html",
          controller: "ProductListCtrl"
        })
        .state('products.show',{
          url: "/:productId",
          views: {
                "@": {
                    templateUrl: "common/product/item.html",
                    controller: "ProductShowCtrl"
                }
          },
          resolve: {
            product: ['$stateParams', 'Restangular', function($stateParams, Restangular){
                var product = Restangular.one('filter/product', $stateParams.productId).get({projection: "fullProduct"})
                return product;
            } ]
          }
        })

    $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';

  }).run(function ($rootScope, CartService,$state){
    CartService.init('cart');
    CartService.clearCart();
     $rootScope.$on('$stateChangeStart', function(event, toState) {
        if(toState.module == 'private' && !$rootScope.authenticated){
             event.preventDefault();
             $state.go('category');
        }
     });
    $rootScope.cart = CartService;
    $rootScope.authenticated = false;
  })
/*  .controller('navigation', function() {});*/
