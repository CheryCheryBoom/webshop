package sombraShop.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import sombraShop.shop.category.Category;
import sombraShop.shop.category.CategoryRepository;
import sombraShop.shop.product.Product;
import sombraShop.shop.product.ProductRepository;

import java.util.HashSet;


@Controller
public class FilterController {

    @Autowired
    CategoryRepository categoryRepository;
    @Autowired
    ProductRepository productRepository;

    @RequestMapping(value = "filter/category/{category}", method = RequestMethod.GET)
    @ResponseBody
    public HashSet<Product> findAllProductsByCategory(@PathVariable String category) {
        Category c = categoryRepository.findByName(category);
        return productRepository.findByCategory(c);
    }

    @RequestMapping(value = "filter/product/{productId}", method = RequestMethod.GET)
    @ResponseBody
    public Product findProductById(@PathVariable String productId) {
        Long id = new Long(productId);
        return productRepository.findOne(id);
    }
}
