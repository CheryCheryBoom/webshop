package sombraShop.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import sombraShop.shop.auth.User;
import sombraShop.shop.auth.UserDetailsServiceImpl;
import sombraShop.shop.auth.UserRepository;

import javax.validation.Valid;

@Controller
public class LoginController {
    @Autowired
    private UserDetailsServiceImpl userDetailsService;
    @Autowired
    private UserRepository userRepository;

    @RequestMapping(value = "/user", method = RequestMethod.POST)
    @ResponseBody()
    public Object login (@Valid @RequestBody User user){
        User requestedUser = userRepository.findByEmail(user.getEmail());
        if (requestedUser != null && (requestedUser.getPassword().compareTo(user.getPassword()) == 0)) {
            UserDetails userDetails = userDetailsService.loadUserByUsername(user.getEmail());
            return HttpStatus.ACCEPTED;
        }
        else return HttpStatus.BAD_REQUEST;
    }
}
