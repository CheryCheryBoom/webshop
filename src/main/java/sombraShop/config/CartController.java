package sombraShop.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import sombraShop.shop.auth.User;
import sombraShop.shop.auth.UserRepository;
import sombraShop.shop.cart.Cart;
import sombraShop.shop.cart.CartRepository;

import javax.validation.Valid;


@Controller
public class CartController {
    @Autowired
    CartRepository cartRepository;


    @RequestMapping(value = "/cart", method = RequestMethod.POST)
    @ResponseBody
    public Object getValue(@Valid @RequestBody Cart requestedCart){
/*        new Cart(requestedCart.getEmail(),requestedCart.getProduct());*/
        cartRepository.save(new Cart(requestedCart.getEmail(),requestedCart.getProduct()));
        return HttpStatus.ACCEPTED;
    }
}
