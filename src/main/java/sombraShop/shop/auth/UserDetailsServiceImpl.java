package sombraShop.shop.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;


@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

/*    @Autowired
    UserDetailsServiceImpl(UserRepository repo){
        User user = new User("user@com.ua", "user");
        repo.save(user);
        userRepository = repo;
    }*/

    @Override
/*    @RequestMapping(value = "/login")*/
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        List<GrantedAuthority> auth = AuthorityUtils.commaSeparatedStringToAuthorityList("ROLE_USER");
        User user = userRepository.findByEmail(email);
        if (user == null) return null;
        return new org.springframework.security.core.userdetails.User(user.getEmail(),user.getPassword(),true,true,true,true,auth);
    }
}