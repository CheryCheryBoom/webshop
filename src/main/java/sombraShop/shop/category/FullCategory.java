package sombraShop.shop.category;

import org.springframework.data.rest.core.config.Projection;


@Projection(name = "fullCategory", types = {Category.class})
public interface FullCategory {
    Long getId();

    String getName();

    String getDescription();

    String getPictureLink();
}
