package sombraShop.shop.category;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;


@RepositoryRestResource(collectionResourceRel = "category", path = "category", excerptProjection = FullCategory.class)
public interface CategoryRepository extends CrudRepository<Category, Long>{
    Category findByName(String name);
}

