package sombraShop.shop.product;

import org.springframework.data.rest.core.config.Projection;
import sombraShop.shop.category.Category;


@Projection(name = "fullProduct", types = {Product.class})
public interface FullProduct {
    Long getId();

    String getName();

    double getPrice();

    String getDescription();

    Category getCategory();

    String getPictureLink();
}
