package sombraShop.shop.product;


/*import org.jboss.logging.annotations.Param;*/
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import sombraShop.shop.category.Category;
import org.springframework.data.repository.query.Param;

import java.util.HashSet;

@RepositoryRestResource(collectionResourceRel = "product", path = "product", excerptProjection = FullProduct.class)
public interface ProductRepository extends CrudRepository<Product, Long> {
/*    @Query("From Product where name =  ?1")*/
    HashSet<Product> findByCategory(Category category);
}
