package sombraShop.shop.cart;

import org.hibernate.validator.constraints.NotEmpty;
import sombraShop.shop.auth.User;
import sombraShop.shop.product.Product;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;

@Entity
public class Cart {
    public Cart(String email, List<Product> products) {
        this.email = email;
        this.products = products;
    }
    private Cart(){};

    @Id
    @GeneratedValue
    private Long id;

    @NotEmpty
    String email;

    @NotEmpty
    @OneToMany
    List<Product> products;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Product> getProduct() {
        return products;
    }

    public void setProduct(List<Product> products) {
        this.products = products;
    }
}
