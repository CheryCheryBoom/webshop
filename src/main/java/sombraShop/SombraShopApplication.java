package sombraShop;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import sombraShop.shop.auth.User;
import sombraShop.shop.auth.UserRepository;
import sombraShop.shop.category.Category;
import sombraShop.shop.category.CategoryRepository;
import sombraShop.shop.product.Product;
import sombraShop.shop.product.ProductRepository;

import javax.annotation.PostConstruct;
import java.security.Principal;

@SpringBootApplication
@RestController
public class SombraShopApplication {

    @Autowired
    CategoryRepository categoryRepository;
    @Autowired
    ProductRepository productRepository;
    @Autowired
    UserRepository userRepository;


    @PostConstruct
    private void someInits(){
        User user = new User("user@com.ua", "user");
        userRepository.save(user);

        Category guns = new Category();
        guns.setName("Guns");
        guns.setDescription("Your neighbors are too loud? You are sick of queues in Privatbank and every night your girl have a headache? Guns can solve any problem!");
        guns.setPictureLink("http://static.hdw.eweb4.com/media/thumbs/1/120/1198131.jpg");

        Category smartPhones = new Category();
        smartPhones.setName("Smartphones");
        smartPhones.setDescription("More than 1000 phones can satisfy the tastes of any buyer. And incredibly low prices!");
        smartPhones.setPictureLink("http://thomasdishaw.com/wp-content/uploads/2015/06/smartphone-collection.jpg");

/*        allCategories.setCategories(smartPhones);
        smartPhones.setParent(allCategories);*/

        Category kitten = new Category();
        kitten.setName("Kitten");
        kitten.setDescription("Dreaming about nice and lovely kittens? This site is all for you. Dosens of kittens of any breed.");
        kitten.setPictureLink("http://hd.wallpaperswide.com/thumbs/kittens-t2.jpg");

        categoryRepository.save(guns);
        categoryRepository.save(smartPhones);
        categoryRepository.save(kitten);

        Product iPhone = new Product("iPhone 5s", 500.00, smartPhones);
        iPhone.setDescription("The iPhone 5 is a touchscreen smartphone developed by Apple Inc. It is the sixth generation of the iPhone, succeeding the iPhone 4S and preceding the iPhone 5S and iPhone 5C.");
        iPhone.setPictureLink("https://geizhals.at/p/999650.jpg");

        Product nokia = new Product("Nokia Lumia 820", 150.00, smartPhones);
        nokia.setDescription("The Nokia Lumia 820 is a high-end smartphone[4] designed, developed and marketed by Nokia. It is the successor to the Lumia 800 and is one of the first Nokia phones to implement Windows Phone 8 alongside the Nokia Lumia 920.");
        nokia.setPictureLink("http://www.ixbt.com/short/images/2012/Sep/700-nokia-lumia-820-red-and-yellow.jpg");

        Product nexus = new Product("Google Nexus 5", 300.00, smartPhones);
        nexus.setDescription("Google manages the design, development, marketing, and support of these devices, but some development and all manufacturing are carried out by partnering original equipment manufacturers (OEMs).");
        nexus.setPictureLink("http://keddr.com/wp-content/uploads/2013/11/google-nexus-5.jpg");

        Product awp = new Product("Arctic Warfare", 18000.00, guns);
        awp.setDescription("The Accuracy International Arctic Warfare rifle is a family of bolt-action sniper rifles designed and manufactured by the British company");
        awp.setPictureLink("http://static.giantbomb.com/uploads/original/11/112064/1633333-awp_render_by_jtbentley.jpg");

        Product blaster = new Product("Stormtrooper rifle", 2000.00, guns);
        blaster.setDescription("God damn blaster created to defeat rebels");
        blaster.setPictureLink("http://blindsquirrelprops.com/wp-content/uploads/2005/03/E11_old_01.jpg");

        Product spitfire = new Product("Spitfire", 5000.00,guns);
        spitfire.setDescription("A flamethrower is a mechanical incendiary device designed to project a long, controllable stream of fire.");
        spitfire.setPictureLink("http://www.thespecialistsltd.com/files/M9A1-7_Flamethrower.jpg");

        Product scotish = new Product("Scotish fold", 200.00, kitten);
        scotish.setDescription("The Scottish Fold is a breed of cat with a natural dominant-gene mutation that affects cartilage throughout the body, causing the ears to fold");
        scotish.setPictureLink("http://buzzsharer.com/wp-content/uploads/2015/05/playful-scottish-fold.jpg");

        Product chartreux = new Product("Chartreux", 250.00, kitten);
        chartreux.setDescription("The Chartreux is a rare breed of domestic cat from France and is recognised by a number of registries around the world");
        chartreux.setPictureLink("http://www.bluecatfarm.com/A%20Doubting%20Belle%20Fleur1.jpg");

        Product manul = new Product("Felis manul", 300.00, kitten);
        manul.setDescription("he Pallas's cat (Otocolobus manul), also called manul, is a small wild cat with a broad but fragmented distribution in the grasslands and montane steppes of Central Asia.");
        manul.setPictureLink("https://pbs.twimg.com/profile_images/1189234161/Manul.jpg");

        productRepository.save(iPhone);
        productRepository.save(nokia);
        productRepository.save(nexus);
        productRepository.save(awp);
        productRepository.save(blaster);
        productRepository.save(spitfire);
        productRepository.save(scotish);
        productRepository.save(chartreux);
        productRepository.save(manul);

    }


    public static void main(String[] args) {
        SpringApplication.run(SombraShopApplication.class, args);
    }
}
